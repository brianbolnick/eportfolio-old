import javax.swing.JOptionPane; //imports class JOptionPane
import java.util.*; 
public class Resume {


/*************************************************************************************************/   
   public void mainMenu () {
     
    String answer = JOptionPane.showInputDialog("What would you like to do?\n\n1 - View Personal Info\n"
            + "2 - View Employment\n3 - View Education\n4 - View Service\n5 - View Other Skills and Interests\n6 - Exit");
    int intAnswer = Integer.parseInt(answer);
    while (intAnswer <= 0 || intAnswer > 6 )
        {
            JOptionPane.showMessageDialog(null, "Sorry, that isn't a valid response.\nPlease try again", "Error Message", 
             JOptionPane.ERROR_MESSAGE); 
            answer = JOptionPane.showInputDialog("What would you like to do?\n\n1 - View Personal Info\n"
            + "2 - View Employment\n3 - View Education\n4 - View Service\n5 - View Other Skills and Interests\n6 - Exit");
            intAnswer = Integer.parseInt(answer);
        }
    
    switch(intAnswer) {
      case 1: 
         showPersonalInfo();
         mainMenu();
         break; 
      case 2:
          Employment emp = new Employment ();
          emp.showEmploymentMenu();
         break;
      case 3:
          Education edu = new Education ();
          edu.showEducationMenu();
          mainMenu();
          break;
      case 4:
          Service srv = new Service ();
          srv.showServiceMenu();
          break;
      case 5: 
          Other other = new Other ();
          other.showOtherMenu();
          mainMenu();
      case 6:
         JOptionPane.showMessageDialog(null, "Thank you for taking the time to view my resume. Goodbye!", 
                 "Thank You", JOptionPane.PLAIN_MESSAGE);
         break;
         
      }//end switch
   }//end mainMenu 
 /*************************************************************************************************************/  
   public void showPersonalInfo () {
   
      String text = "<html><span style='color:black'><strong><em>Brian Bolnick</em></strong></span></html>";
      JOptionPane.showMessageDialog(null, text + "\n641 University Village\nSalt Lake City, UT 84108\n"
              + "brianbolnick@gmail.com\n(801)-839-9266", "Personal Information", JOptionPane.PLAIN_MESSAGE);
      
   
   }//end showPersonalInfo
   
 /*************************************************************************************************************/  
   public static void main (String [] args){
      Resume r = new Resume();
      
      JOptionPane.showMessageDialog(null,"Welcome to my Resume Application!\n\n(Press OK to Continue)\n",
              "Welcome",JOptionPane.PLAIN_MESSAGE);
      r.mainMenu();
     
   }//end main method
}//end class Resume

/*************************************************************************************************************/ 
//                                             Education Class                                               //
/*************************************************************************************************************/ 
class Education {
    
    public static void showEducationMenu() {
    
        JOptionPane.showMessageDialog(null, "<html><strong><em>Salt Lake Community College</em></strong></html>\n" + 
                " Anticipated Graduation - May 2017\n\n" + "•	Bachelor of Science, Computer Science   \n" +
                "•	Emphasis in Software Development    \n" + "•Minor, Business \n" +
                "•	Cumulative GPA 4.0", "Education", JOptionPane.PLAIN_MESSAGE);
    }//end showEducationMenu
}// end class

/*************************************************************************************************************/ 
//                                             Other Class                                               //
/*************************************************************************************************************/ 
class Other {
    
    public void showOtherMenu(){
        
        String title = "<html><strong><em>Other Skills and Interests:</em></strong></html>\n\n";
        JOptionPane.showMessageDialog(null, title + "•	Strong Computer Skills: Microsoft Office proficiency, basic knowledge of Java,\n" +
           "   HTML/CSS, C++, and Python\n" + "•	Strong Language Skills: reading, writing, and speaking Italian fluency, strong\n" +
           "   interpretation skills\n" +"•	Enjoy playing hockey, playing lacrosse, snowboarding, cooking, learning and trying new things",
                "Other 