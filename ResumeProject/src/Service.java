import javax.swing.JOptionPane;
public class Service {
    
   private String term; 
   private String menu = "<html><span style='color:blue'><em>Press Enter to return to Service.</em></span></html>";
   public void showServiceMenu(){
   
      String answer = JOptionPane.showInputDialog("Select a Service to view:\n\n1 - The Church of Jesus Christ of Latter-Day Saints\n"
              + "2 - Special Needs Hockey Organization\n3 - Return to previous menu");
        int intAnswer = Integer.parseInt(answer);
        while (intAnswer <= 0 || intAnswer > 3 )
        {
            JOptionPane.showMessageDialog(null, "Sorry, that isn't a valid response.\nPlease try again", "Error Message", 
             JOptionPane.ERROR_MESSAGE); 
            answer = JOptionPane.showInputDialog("Select a Service to view:\n\n1 - The Church of Jesus Christ of Latter-Day Saints\n"
              + "2 - Special Needs Hockey Organization\n3 - Return to previous menu");
            intAnswer = Integer.parseInt(answer);
        }
      
         switch (intAnswer){
            case 1:
               showLDS();
               break;
            case 2:
               showSNHO();
               break;
            case 3:
               Resume r = new Resume();
               r.mainMenu();
                break;
         }//end switch
   }//end showServiceMenu
   
   public void showLDS() {
       term = "<html><em>March 2010 – March 2012</em></html>";
       String location = "<html><em>Milan, Italy</em></html>";
       
       JOptionPane.showMessageDialog(null, term + "\n" + location + "•	Lived under strict rules of obedience, punctuality, and trust \n" +
        "•	Provided community services to over 100 families and assisted  local food/shelter agencies \n" +
        "•	Supervised and motivated the work of 30+ representatives\n" +
        "•	Led through quantitative goal setting, reviewing past goals, learning new teaching and \n" +
        "   contact methods, and increasing team member morale\n" +
        "•	Developed skills of communication, prioritizing and organization, \n" +
        "   teamwork, time management, attention to detail, and problem solving\n\n" + menu,
         "The Church of Jesus Christ of Latter-Day Saints - Volunteer Representative", JOptionPane.PLAIN_MESSAGE);
       
       showServiceMenu();
   }//end showLDS
   
   public void showSNHO() {
       term = "<html><em>May 2008 – August 2009</em></html>";
       JOptionPane.showMessageDialog(null, term + "\n" + "•	Aided handicapped and disabled persons in athletic involvement", 
               "Special Needs Hockey Organization - Volunteer", JOptionPane.PLAIN_MESSAGE);

       show