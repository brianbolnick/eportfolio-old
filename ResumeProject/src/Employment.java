import javax.swing.JOptionPane;
public class Employment {
    
    private String menu = "<html><span style='color:green'><em>Press Enter to return to Employment.</em></span></html>";
    private String term;

/*************************************************************************/
   public void showEmploymentMenu() {
   
      String answer = JOptionPane.showInputDialog("Select a Company to view:\n\n1 - EMC Corporation\n"
              + "2 - America First Credit Union\n3 - Fossil Store\n4 - Return to previous menu");
        int intAnswer = Integer.parseInt(answer);
        while (intAnswer <= 0 || intAnswer > 4 )
        {
            JOptionPane.showMessageDialog(null, "Sorry, that isn't a valid response.\nPlease try again", "Error Message", 
             JOptionPane.ERROR_MESSAGE); 
            answer = JOptionPane.showInputDialog("Select a Company to view:\n\n1 - EMC Corporation\n"
              + "2 - America First Credit Union\n3 - Fossil Store\n4 - Return to previous menu");
            intAnswer = Integer.parseInt(answer);
        }//end while
      
         switch (intAnswer){
            case 1:
               showEMC();
               break;
            case 2:
               showAFCU();
               break;
            case 3:
               showFossil();
               break;
            case 4:
               Resume r = new Resume();
               r.mainMenu();
         }//end switch
      }//end showEmploymentMenu
/***************************************************************************/      
   public void showEMC(){
      term = "<html><span style='color:black'><em>March 2013 - Present</em></span></html>";
      JOptionPane.showMessageDialog(null, term + "\n\n•	Provide front end non-technical support for internal and external customers"
              + "\n•	Handled 100+ calls weekly resulting  with high customer satisfaction\n"
              + "•	Use problem solving skills to assess, create, research, and update service requests to ensure they "
              + "are assigned to the appropriate product support procedures\n•	Interface with global support teams and "
              + "translate technical problems from the Italian language\n\n" + menu, 
              "EMC Corporation - Customer Support Technician", JOptionPane.PLAIN_MESSAGE);
      showEmploymentMenu();
   }//end showEMC

/***************************************************************************/      
   public void showAFCU(){
       term = "<html><span style='color:black'><em>April 2012 - March 2013</em></span></html>";
       JOptionPane.showMessageDialog(null, term + "\n\n•	Promoted to Level 4 Teller with certification\n"
              + "•	Performed over 16,000 transactions resulting in high customer satisfaction\n"
              + "•	Paid close attention to detail to ensure that all information is correct and accurate\n\n" + menu,
              "America First Credit Union - Teller\n\n", JOptionPane.PLAIN_MESSAGE);
      showEmploymentMenu();
   }//end showAFCU
   
/***************************************************************************/      
   public void showFossil(){
       term = "<html><span style='color:black'><em>April 2012 - September 2012</em></span></html>";
       JOptionPane.showMessageDialog(null, term + "\n\n•	Met sales goals and quotas daily, weekly, and monthly\n"
              + "•	Performed excellent customer service helping the customer according to their needs\n"
              + "•	Maintained knowledge of current sales and promotions policies\n\n" +  menu,
              "Fossil Store - Retail Sales Associate", JOptionPane.PLAIN_MESSAGE);
      showEmploymentMenu();
   }//end showFossil
/**********************************